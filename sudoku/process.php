<head>
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<br>
	<div class="col-lg-5" style="border:1px solid black;">
<?php

//GET CONTENTS OF THE INPUT FILE THROUGH URL
$url=$_POST["url"];
$file=file_get_contents($url);

//INITIALIZE VARIABLES
$length=0;$j=0;$k=0;$prev=0;

/*SEPERATE INPUT STRING INTO VARIOUS SUDOKU PUZZLES - EACH PUZZLE IS STORED INTO AN ARRAY WHOSE FIRST ELEMENT IS THE VALUE OF N FOR THAT PUZZLE, THE COLLECTION OF PUZZLES IS ALSO AN ARRAY, HENCE THE $puzzles IS A 2-D ARRAY
*/
for($i=0;$i<strlen($file);$i++)
{
	if(ord($file[$i])==10||$file[$i]=="\n"||$file[$i]==" ")
	{
		$length=$i-$prev;
		$puzzles[$j][$k]=substr($file,$prev,$length);
		$prev=$i+1;
		$j++;
		$k=0;
	}
	else
	{
		if($file[$i]==';'||$file[$i]==',')
		{
			$length=$i-$prev;
			$puzzles[$j][$k]=substr($file,$prev,$length);
			$prev=$i+1;
			$k++;
		}
		
	}
}
$length=$i-$prev;
$puzzles[$j][$k]=substr($file,$prev,$length);


//THIS LOOP TRAVERSES THROUGH THE 2-D ARRAY FOR VARIOUS CHECKS 
for($i=0;$i<count($puzzles);$i++)
{
	$N=$puzzles[$i][0];//$N = VALUE OF N
	$SUM=0;//EXPECTED SUM ACCORDING TO N
	$flag=0;
	//SUM IS COMPUTED HERE,EX: 1 to 9, SUM = 45
	for($t=1;$t<=$N;$t++)
	{
		$SUM=$SUM+$t;
	}

	//FOR LOOP THROUGH A PARTICULAR PUZZLE
	for($j=1;$j<=$N;$j++)
	{
		//CHECK IF THE NUMBER IS BETWEEN 1 AND N, OTHERWISE SUDOKU IS INVALID
		if($puzzles[$i][$j]<=0||$puzzles[$i][$j]>$N)
		{
			$flag=1;
			break;
		}

		//ROW AND COLUMN SUM MUST BE EQUAL TO EXPECTED SUM
		$row_sum=0;
		$col_sum=0;
		$row_start=(($j-1)*$N)+1;
		$col_start=1;
		for($k=0;$k<$N;$k++)
		{
			$row_sum=$row_sum+$puzzles[$i][$row_start+$k];
			$col_sum=$col_sum+$puzzles[$i][($N*($k))+$j];
		}
		if($col_sum!=$SUM&&$row_sum!=$SUM)
		{
			$flag=1;
			break;
		}
	}

	//IF THE ABOVE CHECKS DIDN"T GO THROUGH, THEN THE SUDOKU IS INVALID
	if($flag==1)
	{
		$result[$i]='False';
	}
	else
	{
		//FORTHER CHECKS : ALSO CHECKED FOR UNIQUENESS OF THE NUMBERS
		$flag2=0;
		$repeat_check=array_count_values($puzzles[$i]);
		if(count($repeat_check)!=$N)
		{
			$flag2=1;
		}
		else
		{
			for($m=1;$m<=$N;$m++)
			{
				if(($repeat_check[$m]!=$N&&$m!=$N)||($repeat_check[$m]!=($N+1)&&$m==$N))
				{
					$flag2=1;
					break;
				}
			}
		}
		if($flag2==1)
		{
			$result[$i]='False';
		}
		else
		{
			$result[$i]='True';
		}
	}

}

//PRINT RESULT
for($i=0;$i<count($result);$i++)
{
	print_r($result[$i]);
	echo "<br>";
}

?>
	</div>
	<div style="clear:both;"></div>
	<div class="col-lg-5">
		<a href="index.php"><button class="btn btn-primary" style="float:right;">BACK</button></a>
	</div>
</body>