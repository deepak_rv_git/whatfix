<head>
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
  	<script>
  		$(document).ready(function(){
  			$('#myModal').modal('show');
    		
    	});
  	</script>
</head>
<body>

	  <!-- Modal -->
	  <div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	      	<form action="process.php" method="POST">	
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Message</h4>
		        </div>
		        <div class="modal-body">
		          <p>Please Enter the url of the Input file.</p>
		          <input name="url" class="form-control" type="text">
		        </div>
		        <div class="modal-footer">
		          <button type="submit" class="btn btn-success">Submit</button><a href="../"><button type="button" class="btn btn-danger">Back</button></a>
		        </div>
	        </form>
	      </div>
	    </div>
	  </div>
	
</body>
<style type="text/css">
	.btn{margin:10px;}
</style>