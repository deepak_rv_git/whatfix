<head>
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<br>
	<div class="col-lg-5" style="border:1px solid black;">
<?php

//GET CONTENTS OF THE INPUT FILE THROUGH URL
$url=$_POST["url"];
$file=file_get_contents($url);

//INITIALIZE VARIABLES
$length=0;$j=0;$k=0;$prev=0;

/*SEPERATE INPUT STRING INTO VARIOUS TRIPS - EACH TRIP IS STORED INTO AN ARRAY WHOSE FIRST ELEMENT IS THE DISTANCE OF THE FIRST CITY AND SECOND IS THE DISTANCE OF THE SECOND CITY AND SO ON.
*/
for($i=0;$i<strlen($file);$i++)
{
	if(ord($file[$i])==10||$file[$i]=="\n"||$file[$i]=="")
	{
		$prev=$i+1;
		$j++;
		$k=0;	
	}
	else
	{
		if($file[$i]==';'||$file[$i]==',')
		{
			$length=$i-$prev;
			if($file[$i]==';')
			{
				$trips[$j][$k]=substr($file,$prev,$length);
				$prev=$i+1;
				$k++;	
			}
			else
			{
				$prev=$i+1;
			}
			
		}
		
	}
}

//SORTING THE DISTANCE ARRAY
function bubbleSort($items) 
{  
  	for($i=(count($items)-1);$i>=0;$i--)
  	{
        for($j=(count($items)-$i-1);$j>0;$j--) 
        {
        	if($items[$j]<$items[$j-1])
        	{
        		$temp=$items[$j];
        		$items[$j]=$items[$j-1];
        		$items[$j-1]=$temp;
        	}
        }
  	}
  	return $items;                 
}

for($i=0;$i<count($trips);$i++)
{
	$trips[$i]=bubbleSort($trips[$i]);
}

//CONVERTING ABSOLUTE DISTANCE TO RELATIVE

function transform($items1)
{
	$items2=$items1;
	$items2[0]=$items1[0];
	for($i=1;$i<count($items1);$i++)
	{
		$items2[$i]=$items1[$i]-$items1[$i-1];
	}
	return $items2;
}

for($i=0;$i<count($trips);$i++)
{
	$trips[$i]=transform($trips[$i]);
}


//PRINT RESULT
for($i=0;$i<count($trips);$i++)
{
	for($j=0;$j<count($trips[$i]);$j++)
	{
		print_r($trips[$i][$j]);
		if($j!=(count($trips[$i])-1))
		{
			echo ",";
		}
	}
	echo "<br>";
}

?>
	</div>
	<div style="clear:both;"></div>
	<div class="col-lg-5">
		<a href="index.php"><button class="btn btn-primary" style="float:right;">BACK</button></a>
	</div>
</body>